Source: pdsh
Section: net
Priority: optional
Maintainer: Brian Pellin <bpellin@debian.org>
Uploaders: tony mancill <tmancill@debian.org>
Build-Depends: debhelper-compat (= 13), libgenders0-dev (>= 1.3-4), libreadline-dev
Standards-Version: 4.6.1.0
VCS-Git: https://salsa.debian.org/bpellin/pdsh.git

Package: pdsh
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, perl, openssh-client | ssh (<< 1:3.8.1p1-9), genders ( >= 1.4-1-1 )
Recommends: ssh-askpass
Homepage: https://software.llnl.gov/repo/#/chaos/pdsh
Description: Efficient rsh-like utility, for using hosts in parallel
 Pdsh is a high-performance, parallel remote shell utility, similar to dsh. 
 It has built-in, thread-safe clients for rsh. Pdsh uses a "sliding window"
 parallel algorithm to conserve socket resources on the initiating node and
 to allow progress to continue while timeouts occur on some connections.
 .
 It makes all parallel connections from one client machine, and attempts to
 keep 32 (default, can be changed on command line) connections to remote
 machines at any given time.  It can run single commands or as an interactive
 shell.
